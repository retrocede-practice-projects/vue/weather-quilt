const process = require('process');
const querystring = require('querystring');
const { schedule } = require('@netlify/functions')
// fauna database
const { Client, query } = require('faunadb');

// date lib
const { DateTime } = require('luxon');

// xhr lib
const axios = require('axios');

const client = new Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
  domain: 'db.fauna.com',
  port: 443,
  scheme: 'https',
})

const handler = async (event) => {
  try {
    console.log('Weather Alert invoked!');
    
    // First, Fetch yesterday's data from NOAAA API.
    const yesterday = getDate();

    console.log(`fetching data for ${yesterday}`)

    const { data } = await axios.post('https://data.rcc-acis.org/StnData', {
      "params": {
        "elems": [
          { "name": "maxt" },
          { "name": "mint" },
          { "name": "pcpn" },
        ],
        "sid": "USW00014922",
        "sDate": yesterday,
        "eDate": yesterday,
      },
      "output": "json",
    });

    console.log('result: ', data);
    console.log('result: ', data.data[0]);
    
    // Second, add yesterday's data to Faunadb
    const [date, max, min, rain] = data.data[0];

    console.log(`${date} max: ${max} min: ${min} rain: ${rain}`);
      
    const item = {
      data: {
        date,
        max: parseInt(max, 10),
        min: parseInt(min, 10),
        rain: parseFloat(rain),
      }
    };

    const faunaResp = await client.query(query.Create(query.Collection('items'), item));
    console.log('success: ', faunaResp);

    // Third, Use twilio to send an SMS to Krystal and I.
    let twilioResponse = await axios.post(`https://api.twilio.com/2010-04-01/Accounts/${process.env.TWILIO_ACCOUNT}/Messages.json`, querystring.stringify({
      To: '+14804652962',
      From: '+16513696525',
      Body: `${date} max: ${max} min: ${min} rain: ${rain}`,
    }), {
      auth: {
        username:  process.env.TWILIO_ACCOUNT,
        password:  process.env.TWILIO_PW,
      }
    });

    console.log('twilioResponse? ', twilioResponse);
    twilioResponse = await axios.post(`https://api.twilio.com/2010-04-01/Accounts/${process.env.TWILIO_ACCOUNT}/Messages.json`, querystring.stringify({
      To: '+14806921568',
      From: '+16513696525',
      Body: `${date} max: ${max} min: ${min} rain: ${rain}`,
    }), {
      auth: {
        username:  process.env.TWILIO_ACCOUNT,
        password:  process.env.TWILIO_PW,
      }
    });
    console.log('twilioResponse? ', twilioResponse);
    
    return {
      statusCode: 200,
      body: JSON.stringify(data)
      // // more keys you can return:
      // headers: { "headerName": "headerValue", ... },
      // isBase64Encoded: true,
    }
  } catch (error) {
    console.log('error: ', error);
    return { statusCode: 500, body: error.toString() }
  }
}

//
// Get Date formatted as string for NOAAA api
//
function getDate() {
  return DateTime.now().setZone('America/Chicago').minus({days: 1}).toISODate();
}

module.exports.handler = schedule("0 16 * * *", handler);
