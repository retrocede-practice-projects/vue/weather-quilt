# Vue 3 + Vite

[![Netlify Status](https://api.netlify.com/api/v1/badges/c306a745-8d6b-462d-9ea5-28c901025cde/deploy-status)](https://app.netlify.com/sites/weather-quilt/deploys)

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)
